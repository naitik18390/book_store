const mongoose = require('mongoose');
const config = require('../config');

mongoose.connect(config.mongo_url, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
}).then(() => {
    console.log("Successfully Connected to MongoDB");
    
}).catch(err => console.log(err));

module.exports = exports = mongoose;
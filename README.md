# book_store


Steps to Configure and access the code.
1) Clone the repository
2) Run npm install
3) Run npm start - This command will start the server on port 7000
4) run API in Postman.
    This is the postman collection Link. Access the api from here.
    https://www.getpostman.com/collections/43ecdd95af72301ef6ab

5) Get localhost:7000/books :-> This will give all books list
6) Get localhost:7000/book/d5a19de9-a5d8-4c2b-8a46-247491a48344  This will give you book detail using UUID number
7) POST localhost:7000/book/add This api will add book in mongo db
8) POST localhost:7000/book/d5a19de9-a5d8-4c2b-8a46-247491a48344/update This api will update book detail using UUID number
9) POST localhost:7000/book/75fc5b7a-ad96-4a6f-bd5b-2ac4e437d501/delete This api will delete book detail from db.

10) Run npm run test to run test cases.
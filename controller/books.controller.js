const validate = require('../validator/validate.js');
const books_biz = require('../biz/books.biz');
const e = require("express");
const booksdetails = new books_biz();
const validate_book = new validate();
class books{
    async addbook(req,res){
        try{
            validate_book.validate_addbook(req.body);
            await booksdetails.create(req.body);
            res.json({
                "success":true,
                "message":"Date Saved Successfully"
            });
        }
        catch(error){
            res.status(201)
            
            res.json({
                success : false,
                error : error.toString() 
            });
            
        }
    }
    async getBookByID(req,res){
        try{
            const book_detail = await booksdetails.getBook(req.params.id);
            res.json(book_detail)
        }catch(error){
            res.status(201)
            
            res.json({
                success : false,
                error : error.toString() 
            });
        }
    }
    async getAllBoks(req,res){
        try{
            const book_detail = await booksdetails.getAllBooks();
            res.json(book_detail)
        }catch(error){
            res.status(201)
            
            res.json({
                success : false,
                error : error.toString() 
            });
        }
    }
    async updateBook(req,res){
        try{
            const book_detail = await booksdetails.updateBookDetails(req.params.id,req.body);
            res.json(book_detail)
        }catch(error){
            res.status(201)
            
            res.json({
                success : false,
                error : error.toString() 
            });
        }
    }
    async deleteBook(req,res){
        try{
            const book_detail = await booksdetails.deleteBookDetails(req.params.id);
            res.json(book_detail)
        }catch(error){
            res.status(201)
            
            res.json({
                success : false,
                error : error.toString() 
            });
        }
    }
    
}
module.exports = books;
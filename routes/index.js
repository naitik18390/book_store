var express = require('express');
var router = express.Router();
const books = require('../controller/books.controller');
const books_controller = new books();
/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});
router.post('/book/add',books_controller.addbook); // Add Book
router.get('/book/:id',books_controller.getBookByID); //Get book Details by UUID
router.get('/books',books_controller.getAllBoks); // Get all book details
router.post('/book/:id/update',books_controller.updateBook); //Update book details by UUID
router.post('/book/:id/delete',books_controller.deleteBook); //Delete book data

module.exports = router;

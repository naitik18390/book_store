const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongo = require('../db/mongo');

const {
    v4: uuidv4
} = require('uuid');

const bookSchema = new Schema({
    uuid: {
        type: String,
        default : uuidv4(),
    },
    
    name:{
        type: String,
        required: true
    },
    releaseDate: {
        type: Date,
        default: Date.now,
        required: true
    },
    authorName: {
        type: String,
        required: true
    }

}, {
    timestamps: true
});



books = mongo.model('books', bookSchema);


module.exports = books;
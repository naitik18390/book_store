class validate{
     validate_addbook(data){
         try{
            let missing_params = [];
            if(!data.name) {
                missing_params.push("Book Name");
            } 
            if(!data.releaseDate) {
                missing_params.push("Release Date")
            } 
            if(!data.authorName) {
                missing_params.push("Author Name")
            }
            if(missing_params.length){
                throw "Params Missing -" + missing_params.toString();
            } 
            return true;
         }catch(e){
            throw e ;
         }
        
    }
    
}
module.exports = validate;
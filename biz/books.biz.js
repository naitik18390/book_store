const books_model = require('../models/books.model');
class books {
    create(data){
        return new Promise ((resolve,reject)=>{
            try{
                const book_save = new books_model(data);
                book_save.save(data,function(err,res){
                    if(err){
                
                        reject (err);
                    }
                
                    resolve(true);
                })
            }catch(e){
                
                reject(e);
            }
        })
    }
    getBook(id){
        return new Promise((resolve,reject)=>{
            try{
                books_model.find({uuid:id},(err,res)=>{
                    if(err) reject(err);
                    if(res[0]){
                        resolve ({
                            success:true,
                            "data":res[0]
                        })
                    }else{
                        resolve({
                            success:false,
                            "data":"No Data Found"
                        });
                    }
                    
                })
            }catch(e){
                reject(e);
            }
        })
    }
    getAllBooks(){
        return new Promise((resolve,reject)=>{
            try{
                books_model.find({},(err,res)=>{
                    if(err) reject(err);
                    if(res.length){
                        resolve ({
                            success:true,
                            "data":res
                        })
                    }else{
                        resolve({
                            success:false,
                            "data":"No Data Found"
                        });
                    }
                    
                })
            }catch(e){
                reject(e);
            }
        })
    }
    updateBookDetails(id,data){
        return new Promise((resolve,reject)=>{
            try{
                const filter = { uuid: id };
                
                books_model.updateOne(filter,data,(err,res)=>{
                    if(err) reject(err);
                    if(res && res.acknowledged){
                        resolve ({
                            success:true,
                            "Message":"Data Updated Successfully"
                        })
                    }else{
                        resolve({
                            success:false,
                            "data":"Update Failed"
                        });
                    }
                    
                })
            }catch(e){
                reject(e);
            }
        })
    }
    deleteBookDetails(id){
        return new Promise((resolve,reject)=>{
            try{
                const filter = { uuid: id };
                
                books_model.deleteOne(filter,(err,res)=>{
                    if(err) reject(err);
                    if(res){
                        resolve ({
                            success:true,
                            "Message":"Data Deleted Successfully"
                        })
                    }else{
                        resolve({
                            success:false,
                            "data":"Delet Operation Failed"
                        });
                    }
                    
                })
            }catch(e){
                reject(e);
            }
        })
    }
}
module.exports = books;
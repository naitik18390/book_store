const books_model = require('../../models/books.model');
const books_biz = require('../../biz/books.biz');
const booksdetails = new books_biz();


const add_data={
    "name":"Who will cry when you die",
    "releaseDate":"2015-07-23",
    "authorName": "Robin Sharma"
}

const update_data={
    
    "releaseDate":"2007-05-03",
    "authorName": "Rober Kiyosaki"
}
describe ('Books Test cases', () => {
	it ('should Add Book', async () => {
        jest.setTimeout(60000);

		const result  = await booksdetails.create(add_data);
		expect(result).not.toBe(null);
		
    })
    it ('should get a Book', async () => {
		
		const response  = await booksdetails.getBook('7b21c74a-ba9e-41d6-b721-6902674d703e');
        console.log(response);
        expect(response).not.toBe(null);
		expect(typeof response).toBe('object');
		expect(response).toHaveProperty('success');
        expect(response.success).toEqual(true);
		expect(response).toHaveProperty('data');
		
    })
    it ('should get all Books', async () => {
		
        const response  = await booksdetails.getAllBooks();
        console.log(response);
        expect(response).not.toBe(null);
		expect(typeof response).toBe('object');
		expect(response).toHaveProperty('success');
        expect(response.success).toEqual(true);
		expect(response).toHaveProperty('data');
		
    })
    it ('should Update book details', async () => {
		
        const response  = await booksdetails.updateBookDetails('7b21c74a-ba9e-41d6-b721-6902674d703e',update_data);
        console.log(response);
        expect(response).not.toBe(null);
		expect(typeof response).toBe('object');
		expect(response).toHaveProperty('success');
        expect(response.success).toEqual(true);
		
		
    })
    it ('should Delete book details', async () => {
		
        const response  = await booksdetails.deleteBookDetails('fe58b591-66bb-4a23-bf22-1246b4c30293');
        console.log(response);
        expect(response).not.toBe(null);
		expect(typeof response).toBe('object');
		expect(response).toHaveProperty('success');
        expect(response.success).toEqual(true);
		
		
    })
})
